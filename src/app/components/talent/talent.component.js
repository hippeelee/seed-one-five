var talent = {
  templateUrl: './talent.html',
  controller: 'TalentController'
};

angular
  .module('components.talent')
  .component('talent', talent)
  .config(function($stateProvider) {
    $stateProvider
      .state('talent', {
        parent: 'app',
        url: '/talent',
        component: 'talent'
      })
  });
