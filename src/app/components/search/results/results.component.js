var results = {
  templateUrl: './results.html',
  controller: 'SearchResultsController'
};

angular
  .module('components.search')
  .component('results', results)
  .config(function ($stateProvider) {
    $stateProvider
      .state('search.results', {
        parent: 'search',
        url: '/search',
        component: 'results'
      });
  });
