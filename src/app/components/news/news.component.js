var news = {
  templateUrl: './news.html',
  controller : 'NewsController'
};

angular
  .module('components.news')
  .component('news', news)
  .config(function ($stateProvider) {
    $stateProvider
      .state('news', {
        parent   : 'app',
        url      : '/news',
        component: 'news'
      })
  });
