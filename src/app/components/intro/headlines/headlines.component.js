var headlines = {
  templateUrl: './headlines.html',
  controller: 'HeadlinesController'
};

angular
  .module('components.intro')
  .component('headlines', headlines)
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('intro.headlines', {
        parent: 'intro',
        url: '/headlines',
        component: 'headlines'
      });
  });
