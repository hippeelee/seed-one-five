angular
  .module('common', [
    'ui.router',
    'angular-loading-bar'
  ])
  .config(function($locationProvider) {
    $locationProvider.html5Mode(true);
  })
  .run(function ($transitions, cfpLoadingBar) {
    $transitions.onStart({}, cfpLoadingBar.start);
    $transitions.onSuccess({}, cfpLoadingBar.complete);
  });
