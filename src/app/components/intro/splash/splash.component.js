var splash = {
  templateUrl: './splash.html',
  controller: 'SplashController'
};

angular
  .module('components.intro')
  .component('splash', splash)
  .config(function ($stateProvider) {
    $stateProvider
      .state('intro.splash', {
        parent: 'intro',
        url: '/splash',
        component: 'splash'
    });
  });
