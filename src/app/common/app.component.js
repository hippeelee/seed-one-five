var app = {
  templateUrl: './app.html',
  controller: 'AppController'
};

angular
  .module('common')
  .component('app', app)
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        redirectTo: 'intro',
        url: '',
        component: 'app'
      });
    $urlRouterProvider.otherwise('/');
  });
