var gallery = {
  templateUrl: './gallery.html',
  controller: 'GalleryController'
};

angular
  .module('components.intro')
  .component('gallery', gallery)
  .config(function ($stateProvider) {
    $stateProvider
      .state('intro.gallery', {
        parent: 'intro',
        url: '/gallery',
        component: 'gallery'
      });
  });
