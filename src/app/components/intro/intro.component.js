var intro = {
  templateUrl: './intro.html'
};

angular
  .module('components.intro')
  .component('intro', intro)
  .config(function ($stateProvider) {
    $stateProvider
      .state('intro', {
        parent: 'app',
        redirectTo: 'intro.splash',
        url: '',
        template: '<div ui-view></div>'
      });
  });
