var search = {
  templateUrl: './search.html'
};

angular
  .module('components.search')
  .component('search', search)
  .config(function ($stateProvider) {
    $stateProvider
      .state('search', {
        parent: 'app',
        redirectTo: 'search.results',
        url: '',
        template: '<div ui-view></div>'
      });
  });
