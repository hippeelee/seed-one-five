angular
  .module('components', [
    'components.intro',
    'components.news',
    'components.search',
    'components.talent'
  ]);
